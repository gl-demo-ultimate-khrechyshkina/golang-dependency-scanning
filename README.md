# Custom ruleset example for a Go project


## Summary

There are 5 vulnerabilities which the project will be addressing using custom rulesets (check comments in code for the specific vulnerabilities).

- my-semgrep-rules.yml
  - CWE-287 - Improper Authentication: openai-empty-secret
  - CWE-319 - HTTP server without TLS: use-tls

- sast-rules: https://gitlab.com/gitlab-org/security-products/sast-rules/-/tree/main/go?ref_type=heads
  - CWE-409 - [Improper handling of highly compressed data](https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/go/filesystem/rule-decompression-bomb.yml)
  - CWE-310 - [Use of deprecated TLS version](https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/go/crypto/rule-tlsversion.yml)
  - CWE-400 - [Uncontrolled resource consumption (Slowloris)](https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/go/http/rule-http-serve.yml)


The `.gitlab/sast-ruleset.toml` file will:
 - Use GitLab Managed Rules for golang (`/rules/gosec.yml` in the semgrep analyzer image)
 - Add 2 extra rules via `my-semgrep-rules.yml` which are not present in `/rules/gosec.yml` (`.gitlab/sast-ruleset.toml`)
 - Disable 1 rule CWE-409.
 - The results will be that 4 vulnerabilities are reported: CWE-287, CWE-319, CWE-310 and CWE-400

package main

import (
	"compress/gzip"
	"crypto/tls"
	"database/sql"
	"io"
	"log"
	"net/http"
	"os"
	"time"
	"github.com/sashabaranov/go-openai"

)


openai.NewClient("") //CWE-287 custom rule | empty openai credentials

func decompressionBomb() {
	f, err := os.Open("some.gz")
	if err != nil {
		log.Fatal(err)
	}

	r, err := gzip.NewReader(f)
	if err != nil {
		log.Fatal(err)
	}

	// use limitedReader to stop copying after 1 MB
	if _, err := io.Copy(os.Stdout, r); err != nil { //CWE 409 - GitLab rule | consider using a limit reader
		log.Fatal(err)
	}

}


func main() {

	mysqlConnect()

	cert, err := tls.LoadX509KeyPair("server.crt", "server.key")
	if err != nil {
		log.Fatal(err)
	}

	cfg := &tls.Config{Certificates: []tls.Certificate{cert}, MinVersion: tls.VersionTLS11} //CWE-310 - GitLab rule | tls.VersionTLS11 and tls.VersionTLS12 will introduce this vuln, use tls.VersionTLS13 to fix
	//server1
	srv := &http.Server{
		Addr:              ":8999",
		TLSConfig:         cfg,
		ReadTimeout:       time.Minute,
		WriteTimeout:      time.Minute,
		IdleTimeout:       30 * time.Second,
		ReadHeaderTimeout: 15 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())

	//server 2
	// CWE-400 - GitLab Rule | no timeouts defined like //server1
	if err := http.ListenAndServe(":8080", nil); err != nil { //CWE-319 - custom rule | use http.ListenAndServeTLS instead
		log.Fatal(err)
	}

	decompressionBomb()
}
